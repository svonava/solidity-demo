pragma solidity ^0.4.24;

contract SimpleStorage {
  // Make sure that addressWhitelist has enough addresses to chose the voters from.
  uint constant voterCount = 3;
  uint8 constant maxGuess = 10;
  
  // Whitelist of all available voters.
  address[] public addressWhitelist =  [
    0x7d00FeF2bC29111339029B0996d209f81CfC7f7a,
    0x6822db9Ba85edFe776F0B61A6C14873fab4E584F,
    0x97cA65cF98ce299D00409b312f99521e9D3923B3,
    0x5200Dcc87816dE151da9F4C89A3BAE9A974faE37,
    0x6F1E077EeDa60d11c2052B273D76C0297a64f8D4,
    0xD22c031DCE30d71F0d870C1271F00656e23d1736,
    0x4d52911242DeA30E45fD910b5820C2357B136CC7,
    0xB5E6094F6B8b36bc78505F26D5739B5F9D062132,
    0x456EFF5aCe15949bbA7eE56dF74c07b8242e7FEc];

  enum DealStatus {CREATED, APPROVED, REJECTED, SETTLED}
  struct Deal {
    uint dealId;
    uint voteStart;
    address owner;
    // Addresses chosen randomly from addressWhitelist.
    address[voterCount] voters;
    // Voter stakes.
    // Note: Not using mapping(address => uint) because that's PITA to return to web3js.
    uint[voterCount] bids;
    // Score guess in the range [0, maxGuess].
    // Note: Not using mapping(address => uint8) because that's PITA to return to web3js.
    uint8[voterCount] guesses;
    uint32 guess_sum;

    DealStatus status;
  }
  // State maintained to enforce the deal acceptance invariant (2/3th of deals rejected, 1/3th accepted).
  uint32[] private sortedGuesses;

  // Note: This should be private and the 'listDeals()' method should only list the deals after voting is complete.
  // Note: Voters should find out that they'll have to vote on a deal by emmiting an event at voter selection.
  Deal[] private deals;
  function newDeal() public {
    Deal memory deal;
    deal.dealId = deals.length;
    deal.voteStart = block.timestamp;
    deal.voters = choose();
    deal.status = DealStatus.CREATED;
    deal.owner = msg.sender;
    deals.push(deal);
  }

  // Breaks the Deal into primitive types, to transmit to web3js.
  // Note: address[][] not yet supported.
  function listDeals() public view returns(uint[], address[], address[voterCount][], uint[voterCount][], uint8[voterCount][], uint[]) {
    uint[] memory dealIds = new uint[](deals.length);
    address[] memory owners = new address[](deals.length);
    address[voterCount][] memory voters = new address[voterCount][](deals.length);
    uint[voterCount][] memory bids = new uint[voterCount][](deals.length);
    uint8[voterCount][] memory guesses = new uint8[voterCount][](deals.length);
    uint[] memory statuses = new uint[](deals.length);
    for (uint i = 0; i < deals.length; i++) {
      Deal storage deal = deals[i];
      dealIds[i] = deal.dealId;
      owners[i] = deal.owner;
      voters[i] = deal.voters;
      bids[i] = deal.bids;
      guesses[i] = deal.guesses;
      statuses[i] = uint(deal.status);
    }
    return (dealIds, owners, voters, bids, guesses, statuses);
  }

  function submitBid(uint dealId, uint8 guess) public payable {
    // 1. Check preconditions.
    require(msg.value > 0, 'Bid must be positive.');  // Bid value.
    require(dealId >= 0 && dealId < deals.length, 'Invalid DealId.');
    require(guess >=0 && guess <= maxGuess, 'Guess must be in [0,10].');
    Deal storage deal = deals[dealId];
    uint eligibleIndex = 0;
    for (; eligibleIndex < voterCount; eligibleIndex++) {
      if (deal.voters[eligibleIndex] == msg.sender) { 
        break;
      }
    }
    require(eligibleIndex < voterCount, 'Bidder not eligible for the deal.');
    require(deal.bids[eligibleIndex] == 0 && deal.guesses[eligibleIndex] == 0,
            'One address can not submit multiple bids.');
    // 2. Store bid & guess.
    deal.bids[eligibleIndex] = msg.value;
    deal.guesses[eligibleIndex] = guess;
    // 3. Finalize deal.
    // TODO: Consider passing the deal reference around instead, to avoid having to require() again.
    finalizeDeal(dealId);
  }

  // Represents a liquidity event for dealId.
  function acquire(uint dealId) public payable {
    require(msg.value > 0, 'Value must be positive.');  // Acquisition price.
    require(dealId >= 0 && dealId < deals.length, 'Invalid DealId.');
    Deal storage deal = deals[dealId];
    require(deal.status == DealStatus.APPROVED, 'Invalid deal status.');
    // Distribute value to voters, proportional to bid.
    // TODO: Fix rounding issues.
    uint total_payout = msg.value;
    uint bid_sum = 0;
    uint max_bid = 0;
    uint max_bid_index = 0;
    for (uint i = 0; i < voterCount; i++) {
      bid_sum += deal.bids[i];
      if (max_bid < deal.bids[i]) {
        max_bid = deal.bids[i];
        max_bid_index = i;
      }
    }

    for (uint j = 0; j < voterCount; j++) {
      uint voter_payout = (msg.value * bid_sum) / deal.bids[j];
      // BUG: Can't add up deal.bids[j]+voter_payout for some reason. Overflow? Should not be, as all types are 2^256.
      deal.voters[j].transfer(deal.bids[j] /*+ voter_payout*/);
      total_payout -= voter_payout;
    }
    // Check if we have some rounding reminder, if yes transfer to the highest bidder.
    // Break ties by order in the whitelist.
    // BUG: Can't send 2 transactions to the same address it seems.
    //if (total_payout > 0) {
    //  deal.voters[max_bid_index].transfer(total_payout);
    //}
    deal.status = DealStatus.SETTLED;
  }

  function finalizeDeal(uint dealId) private {
    require(dealId >= 0 && dealId < deals.length, 'Invalid DealId.');
    Deal storage deal = deals[dealId];
    require(deal.status == DealStatus.CREATED, 'Invalid deal status.');
    bool isVotingFinished = true;
    for (uint i = 0; i < voterCount; i++) {
      if (deal.bids[i] == 0) {
        isVotingFinished = false;
        break;
      }
    }
    if (!isVotingFinished) {
      return;
    }
    deal.status = updateGuessesAndGetStatus(dealId);
    // If the deal got rejected, return the bids.
    if (deal.status == DealStatus.REJECTED) {
      for (i = 0; i < voterCount; i++) {
        deal.voters[i].transfer(deal.bids[i]);
      }
    }
  }

  function updateGuessesAndGetStatus(uint dealId) private returns (DealStatus) {
    require(dealId >= 0 && dealId < deals.length, 'Invalid DealId.');
    Deal storage deal = deals[dealId];
    deal.guess_sum = 0;
    for (uint i = 0; i < voterCount; i++) {
      deal.guess_sum += deal.guesses[i];
    }
    // TODO: Reject the deal if variance coefficient (var/mean) is too high (voter disagreement).
    sortedGuesses.push(deal.guess_sum);
    uint position = sortedGuesses.length-1;
    for (;position >= 1; position--) {
      // >= to also traverse the equal elements (be consistent about accepting or rejecting a given score all else being equal).
      if (sortedGuesses[position-1] >= sortedGuesses[position]) {
        // Swap.
        (sortedGuesses[position-1], sortedGuesses[position]) = (sortedGuesses[position], sortedGuesses[position-1]);
      } else {
        // Final position found.
        break;
      }
    }
    // Approve the deal if position >= 2/3th percentile of sortedGuesses.
    return 3 * position >= sortedGuesses.length * 2 ? DealStatus.APPROVED : DealStatus.REJECTED;
  }


  // TODO: Audit for attack vectors.
  function random() private view returns (uint256) {
   return uint256(keccak256(abi.encodePacked(block.timestamp, block.difficulty)));
  }

  // Return n samples without repetition from [0, pool).
  // Runtime: O(n^2) - assumes we are chosing a few voters from a large pool.
  // Space: O(n)
  function get_indices(uint pool, uint n, uint rand) private pure returns (uint[]) {
    require(pool > n, 'Insufficient index pool size.');
    require(n > 0, 'Chosen index count must be positive.');

    uint[] memory indices = new uint[](n);
    for (uint i = 0; i < n; i++) {
      // Check if we have enough bits in rand to extract the next random index.
      // Note: This stopping condition is safe, but sub-optimal.
      require(rand >= pool, 'Index pool size too big.');
      // Convert trailing bits into a random index.
      uint rank = rand % (pool-i);
      // Remove trailing bits.
      rand /= pool;

      for (uint j = 0; j < i; j++) {
        if (indices[j] <= rank) {
          rank++;
        }
      }
      indices[i] = rank;
      // Bubble the rank backwards to keep indices sorted.
      for (uint k = i; k > 0; k--) {
        if (indices[k-1] > indices[k]) {
          (indices[k-1], indices[k]) = (indices[k], indices[k-1]);
        } else {
          break;
        }
      }
    }
    return indices;
  }

  // Choose n random addresses from addressWhitelist and return.
  function choose() private view returns (address[voterCount]){
    address[voterCount] memory chosen;
    uint[] memory indices = get_indices(addressWhitelist.length, voterCount, random());
    for (uint i = 0; i < voterCount; i++) {
      chosen[i] = addressWhitelist[indices[i]];
    }
    return chosen;
  }
}
