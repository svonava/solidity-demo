Welcome to a small Solidity Contract DApp demo built using:

*   [Truffle](https://truffleframework.com/): Ethereum development framework.
*   [Ganache](https://truffleframework.com/ganache): Local development Ethereum node.
*   [Web3js](https://github.com/ethereum/web3.js/): Javascript API for interacting with Ethereum networks.
*   [ReactJS](https://reactjs.org/): JS library for UI rendering.

As a starting point, this demo was cloned from the [React Box from Truffle](https://github.com/truffle-box/react-box). See the details there for how to run it.

## Code structure
The most interesting files are:

### contracts/SimpleStorage.sol
The solidity code implementing the contract.

General operation (ordered):

+ **newDeal()** is used to generate a new Deal - uppon creation  we chose a random subset of the *addressWhitelist* as *voters*.

   Randomness in ETH is tricky, we obtain a hash of block timestamp and difficulty and then convert the 256b bit into a set of whitelist indices (without repetition) using **get_indices()**.
   Obviously there is an upper-bound on the whitelist length defined by the random bits we have available (could be improved with a [LCG](https://en.wikipedia.org/wiki/Linear_congruential_generator) or similar.
   Attack vector have to be examined here (e.g. from the miners).
   
+ **listDeals()** can be use any time to obtain all Deals stored in the contract in a form that's easy to transmit to web3js.
+ **submitBid()** each *voter* can submit a bid for each contract they have been chosen for. A bid is an ETH value (stake) accompanied by a score *guess* which aims to approximate what the average guess of the other voters will be.
+ **finalizeDeal()** determines if a vote has been completed and if yes, it resolves and assigns the next state of the Deal (REJECTED or APPROVED).

   The algorithm used maintains a 1/3 approval rate across all deals with completed votes with deals being sorted by the average guess.
   For efficiency, the contract maintains an array of **sortedGuesses** and uses it to quickly determine the 2/3th percentile to threshold the deals in **updateGuessesAndGetStatus()**.
   Ideally we also reject deals with high variance in their guesses, which could indicate a problematic deal. This is not implemented yet.
   
+ **acquire()** is used when a deal is liquidated to accept a payment and distribute it to the voters, proportional to their stakes. Ideally they are also rewarded for the accuracy of their guess, but this is not implemented.

Note: Some of this optimization could be premature, one would have to profile & load test this properly to find the right simplicity & efficiency tradeoff.

### src/App.js
The react components & UI rendering code. The UI generated by this file allows interaction with the SimpleStorage contract defined above.
