import React, { Component } from 'react'
import SimpleStorageContract from '../build/contracts/SimpleStorage.json'
import getWeb3 from './utils/getWeb3'

import './css/oswald.css'
import './css/open-sans.css'
import './css/pure-min.css'
import './App.css'

// Displays a deal. Tied to data handled in:
// - App.handleListDeals()
// - SimpleStorageContract.listDeals()
class DealTableRow extends Component {
  render() {
    const { id, owner, voters, bids, guesses, deal_status } = this.props.deal;
    return (
      <div className="pure-u-1-1" style={{backgroundColor: deal_status === 2?'red': deal_status === 1?'green': deal_status === 3?'purple':'gray'}}>
        <div className="pure-u-1-1">
              <div className='pure-u-1-5 l-box'>{id}</div>
              <div className='pure-u-2-5 l-box'><b>Owner:</b><br/>{owner}</div>
        </div>
        <div className="pure-u-1-1">
          {voters.map((voter, i) => {
          return (
            <div key={i} className="pure-u-1-1" >
              <div className='pure-u-1-5 l-box'></div>
              <div className='pure-u-2-5 l-box'>{!i? 'Voters:' : ''}{!i? <br/>:''}{voter}</div>
              <div className='pure-u-1-5 l-box'>{!i? <br/>:''}{bids[i]}</div>
              <div className='pure-u-1-5 l-box'>{!i? <br/>:''}{guesses[i]}</div>
            </div>);
           })
          }
        </div>
      </div>
        );
  }
}

// Main component representing the whole client.
// TODO: Could use breaking into more granular components.
class App extends Component {
  constructor(props) {
    super(props)

    this.state = {
      storageValue: 0,
      simpleStorageInstance: null,
      deals: [],
      web3: null
    }

    // This binding is necessary to make `this` work in the callbacks.
    this.handleNewDeal = this.handleNewDeal.bind(this);
    this.handleListDeals = this.handleListDeals.bind(this);
    this.handleSubmitBid = this.handleSubmitBid.bind(this);
    this.handleAcquire = this.handleAcquire.bind(this);
  }

  componentWillMount() {
    // Get network provider and web3 instance.
    // See utils/getWeb3 for more info.

    getWeb3
    .then(results => {
      this.setState({
        web3: results.web3
      })
      // Instantiate contract once web3 provided.
      this.initContract();
    })
    .catch(() => {
      console.log('Error finding web3.')
    })
  }

  initContract() {
    const contract = require('truffle-contract')
    const simpleStorage = contract(SimpleStorageContract)
    simpleStorage.setProvider(this.state.web3.currentProvider)

    // Capture the contract instance. Also shows how to get accounts (in case
    // we wanted to show balances in the app).
    this.state.web3.eth.getAccounts((error, accounts) => {
      simpleStorage.deployed().then((instance) => {
        this.setState({simpleStorageInstance: instance});
        this.handleListDeals();
      })
    })
  }

  handleListDeals() {
    var instance = this.state.simpleStorageInstance;
    this.state.web3.eth.getAccounts((error, accounts) => {
      // Account[0] is always paying for the deal listing. Probably this should instead
      // be the current MetaMask connected account.
      // Note: We use 1e6 GAS everywhere, instead of using the web3js mechanism to estimate
      // a reasonable value.
      instance.listDeals({from: accounts[0], gas: 1000000}).then((result) => {
        var deals = [];
        for (var i = 0; i < result[0].length; i++) {
          deals.push({
            id: result[0][i].toNumber(),
            owner: result[1][i],
            voters: result[2][i],
            bids: result[3][i].map(x => this.state.web3.fromWei(x, 'ether').toNumber()),
            guesses: result[4][i].map(x => x.toNumber()),
            deal_status: result[5][i].toNumber(),
          });
        }
        this.setState({deals: deals});
      });
    });
  }

  /*
  ============ EVENT HANDLERS ============
  */
  handleNewDeal(event) {
    event.preventDefault();
    const data = new FormData(event.target);
    var address = data.get('address').trim();
    console.log(address);
    var instance = this.state.simpleStorageInstance;
    instance.newDeal({from: address, gas: 1000000}).then((result) => {
      this.handleListDeals();
    });
  }

  handleSubmitBid(event) {
    event.preventDefault();
    const data = new FormData(event.target);
    var instance = this.state.simpleStorageInstance;
    var dealId = parseInt(data.get('dealId'), 10);
    var address = data.get('address').trim();
    var bid = this.state.web3.toWei(parseFloat(data.get('bid')), 'ether');
    var guess = parseInt(data.get('guess'), 10);

    console.log('submitting bid: ' + dealId + ' ' + guess + ' ' + address + ' ' + bid);
    instance.submitBid(dealId, guess, {from: address, value: bid, gas: 1000000}).then((result) => {
      this.handleListDeals();
    });
  }
  
  handleAcquire(event) {
    event.preventDefault();
    const data = new FormData(event.target);
    var instance = this.state.simpleStorageInstance;
    var dealId = parseInt(data.get('dealId'), 10);
    var address = data.get('address').trim();
    var amount = this.state.web3.toWei(parseFloat(data.get('amount')), 'ether');
    console.log('acquiring: ' + dealId + ' ' + address + ' ' + amount);
    instance.acquire(dealId, {from: address, value: amount, gas: 1000000}).then((result) => {
      this.handleListDeals();
    });
  }

  render() {
    return (
      <div className="App">
        <nav className="navbar pure-menu pure-menu-horizontal">
           <span className="pure-menu-heading">Deal machine</span>
           <span className="pure-menu-heading"> Contract status: {
              this.state.simpleStorageInstance != null? 'Connected': 'Connecting...'} </span>
        </nav>

        <main className="container">
          <div className="pure-g">
              <div className="pure-u-1-1">
               <form className="pure-form pure-g" onSubmit={this.handleNewDeal}>
                 <div className="pure-u-2-5"><input className="pure-input-1" type="text" name="address" placeholder="Owner address" required></input></div>
                 <button type="submit" className="pure-button pure-button-primary">New deal</button>
               </form>
              </div>
              <div className="pure-u-1-1">
               <form className="pure-form pure-g" onSubmit={this.handleSubmitBid}>
                 <div className="pure-u-1-8"><input className="pure-input-1" type="text" name="dealId" placeholder="Deal ID" required></input></div>
                 <div className="pure-u-2-5"><input className="pure-input-1" type="text" name="address" placeholder="Sender address" required></input></div>
                 <div className="pure-u-1-8"><input className="pure-input-1" type="text" name="bid" placeholder="Bid in ETH [decimal]" required></input></div>
                 <div className="pure-u-1-8"><input className="pure-input-1" type="text" name="guess" placeholder="Score [0, 10]" required></input></div>
                 <div className="pure-u-1-8"><button type="submit" className="pure-button pure-button-primary">Submit</button></div>
               </form>
              </div>
              <div className="pure-u-1-1">
               <form className="pure-form pure-g" onSubmit={this.handleAcquire}>
                 <div className="pure-u-1-8"><input className="pure-input-1" type="text" name="dealId" placeholder="Deal ID" required></input></div>
                 <div className="pure-u-2-5"><input className="pure-input-1" type="text" name="address" placeholder="Acquisitor" required></input></div>
                 <div className="pure-u-1-8"><input className="pure-input-1" type="text" name="amount" placeholder="Price ETH [decimal]" required></input></div>
                 <div className="pure-u-1-8"><button type="submit" className="pure-button pure-button-primary">Acquire</button></div>
               </form>
              </div>
              <div className="pure-u-1-1">
                <div className="pure-u-1-5">Deal Id</div>
                <div className="pure-u-2-5"></div>
                <div className="pure-u-1-5">Bids</div>
                <div className="pure-u-1-5">Guesses</div>
              </div>
              <div className="pure-u-1-1">
                  { this.state.deals.map(deal => {
                      return <DealTableRow key={deal.id} deal={deal}/>;
                    }) 
                  }
              </div>
          </div>
        </main>
      </div>
    );
  }
}

export default App
